import java.util.Scanner;

public class SeatingChartExample {
    public static void main(String[] args) {
        char[][] seatingChart = {
            {' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' '}
        };

        int numRows = seatingChart.length;
        int numCols = seatingChart[0].length;

        int bookedSeats = 0;
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                if (seatingChart[row][col] == 'X') {
                    bookedSeats++;
                }
            }
        }

        Scanner scanner = new Scanner(System.in);

        boolean done = false;
        while (!done) {
            System.out.println("Seating Chart:");
            for (int row = 0; row < numRows; row++) {
                System.out.print(row + 1 + " ");
                for (int col = 0; col < numCols; col++) {
                    System.out.print("[" + seatingChart[row][col] + "] ");
                }
                System.out.println();
            }

            System.out.println("\nNumber of booked seats: " + bookedSeats);
            System.out.println("Number of available seats: " + (numRows * numCols - bookedSeats));

            System.out.print("\nEnter the row and column of the seat you want to book (e.g. 1 4), or enter 'q' to quit: ");
            String input = scanner.next();

            if (input.equalsIgnoreCase("q")) {
                done = true;
            } else {
                int selectedRow = Integer.parseInt(input) - 1;
                int selectedCol = scanner.nextInt() - 1;

                if (selectedRow >= 0 && selectedRow < numRows && selectedCol >= 0 && selectedCol < numCols) {
                    if (seatingChart[selectedRow][selectedCol] == ' ') {
                        seatingChart[selectedRow][selectedCol] = 'X';
                        bookedSeats++;
                        System.out.println("Seat booked successfully!");
                    } else {
                        System.out.println("Seat is already booked.");
                    }
                } else {
                    System.out.println("Invalid seat selection.");
                }
            }
        }

        System.out.println("Thank you for using the Seating Chart program!");
        scanner.close();
    }
}