import java.util.Scanner;

public class MovieTicketBooking {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Sample data for theaters, movies, and timings
        String[] theaters = {"Theater A", "Theater B"};
        String[] movies = {"Movie 1", "Movie 2", "Movie 3"};
        String[] timings = {"10:00 AM", "02:00 PM", "06:00 PM"};
        String[] seatType = {"Normal", "3D"};
        System.out.println("Welcome to the Movie Ticket Booking System!");

        // Display available theaters
        System.out.println("Available Theaters:");
        for (int i = 0; i < theaters.length; i++) {
            System.out.println((i + 1) + ". " + theaters[i]);
        }

        System.out.print("Select a theater: ");
        int theaterChoice = scanner.nextInt() - 1;
        scanner.nextLine(); // Consume newline

        // Display available movies
        System.out.println("\nAvailable Movies:");
        for (int i = 0; i < movies.length; i++) {
            System.out.println((i + 1) + ". " + movies[i]);
        }

        System.out.print("Select a movie: ");
        int movieChoice = scanner.nextInt() - 1;
        scanner.nextLine(); // Consume newline

        // Display available timings
        System.out.println("\nAvailable Timings:");
        for (int i = 0; i < timings.length; i++) {
            System.out.println((i + 1) + ". " + timings[i]);
        }

        System.out.print("Select a timing: ");
        int timingChoice = scanner.nextInt() - 1;
        scanner.nextLine(); // Consume newline

        // Get number of people
        System.out.print("Enter number of people: ");
        int numPeople = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        
        // Get membership status
        System.out.println("Are you a member? (Yes/No): ");
        String isMember = scanner.nextLine();
        
        // Get soundtrack choice
        System.out.println("Do you want a soundtrack? (Yes/No): ");
        String hasSoundtrack = scanner.nextLine();

        // Get seat type
        
        
        for (int i = 0; i < seatType.length; i++) {
            System.out.println((i + 1) + ". " + seatType[i]);
        }
        System.out.print("Select a timing: ");
        System.out.print("Enter seat type (Standard/3D): ");
        int seatChoice = scanner.nextInt() - 1;
        scanner.nextLine(); // Consume newline
        
        char[][] seatingChart = {
            {' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' '}
        };

        int numRows = seatingChart.length;
        int numCols = seatingChart[0].length;

        int bookedSeats = 0;
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                if (seatingChart[row][col] == 'X') {
                    bookedSeats++;
                }
            }
        }

        for (int i = 0; i < numPeople; i++) {
            System.out.println("Seating Chart:");
            for (int row = 0; row < numRows; row++) {
                System.out.print(row + 1 + " ");
                for (int col = 0; col < numCols; col++) {
                    System.out.print("[" + seatingChart[row][col] + "] ");
                }
                System.out.println();
            }

            System.out.println("\nNumber of booked seats: " + bookedSeats);
            System.out.println("Number of available seats: " + (numRows * numCols - bookedSeats));

            System.out.print("\nEnter the row and column of the seat you want to book (e.g. 1 4: ");
            String input = scanner.next();

            int selectedRow = Integer.parseInt(input) - 1;
            int selectedCol = scanner.nextInt() - 1;

            if (selectedRow >= 0 && selectedRow < numRows && selectedCol >= 0 && selectedCol < numCols) {
                if (seatingChart[selectedRow][selectedCol] == ' ') {
                    seatingChart[selectedRow][selectedCol] = 'X';
                    bookedSeats++;
                    System.out.println("Seat booked successfully!");
                } else {
                    System.out.println("Seat is already booked.");
                }
            } else {
                System.out.println("Invalid seat selection.");
            }

        }

        System.out.println("Thank you for using the Seating Chart program!");

        // Display booking summary
        System.out.println("\nBooking Summary:");
        System.out.println("Theater: " + theaters[theaterChoice]);
        System.out.println("Movie: " + movies[movieChoice]);
        System.out.println("Timing: " + timings[timingChoice]);
        System.out.println("Number of People: " + numPeople);
        System.out.println("Seat Type: " + seatType[seatChoice]);
        
        System.out.println("Membership: " + isMember);
        System.out.println("Soundtrack: " + hasSoundtrack);

        // Calculate and display total amount (sample calculation)
        double ticketPrice = seatType.equals("3D") ? 15.0 : 10.0;
        double totalPrice = ticketPrice * numPeople;

        System.out.println("Total Price: $" + totalPrice);

        scanner.close();
    }
}