//import java.sql.Array;

public class ArrayManipulation {
    public static void main(String[] args) {
        
        //- An integer array named "numbers" with the values {5, 8, 3, 2, 7}.
        int[] numbers = {5, 8, 3, 2, 7};

        //- A string array named "names" with the values {"Alice", "Bob", "Charlie", "David"}.
        String[] names = {"Alice", "Bob", "Charlie", "David"};

        //- A double array named "values" with a length of 4 (do not initialize the elements yet).
        double[] values = new double[4];
        
        //3. Print the elements of the "numbers" array using a for loop.
        for(int i = 0; i < numbers.length; i++){

            System.out.println(numbers[i]);

        }

        //4. Print the elements of the "names" array using a for-each loop.
        for( String n : names){

            System.out.println(n);

        }

        //5. Initialize the elements of the "values" array with any four decimal values of your choice.
        values[0] = 1.2;
        values[1] = 2.3;
        values[2] = 3.4;
        values[3] = 4.5;

        //6. Calculate and print the sum of all elements in the "numbers" array.
        System.out.println();
        for(int i = 0; i < values.length; i++){
            System.out.println(values[i]);
        }

        //7. Find and print the maximum value in the "values" array.
        double max = Double.MIN_VALUE;
        for (double value : values) {
            if (value > max) {
                max = value;
            }
        }
        System.out.println("Maximum value in the 'values' array : " + max);

        //8. Create a new string array named "reversedNames" with the same length as the "names" array.
        String[] reversedNames = new String[names.length];
        
        //- Fill the "reversedNames" array with the elements of the "names" array in reverse order.
        //เติมอาร์เรย์ "reversedNames" ด้วยองค์ประกอบของอาร์เรย์ "ชื่อ" ในลำดับย้อนกลับ
        for (int i = 0; i < names.length; i++) {
            reversedNames[i] = names[names.length - 1 - i];
        }

        //- Print the elements of the "reversedNames" array.
        //พิมพ์องค์ประกอบของอาร์เรย์ "reversedNames"
        for (String reversedName : reversedNames) {
            System.out.print(reversedName + " ");
        }
        System.out.println();

        //9. BONUS: Sort the "numbers" array in ascending order using any sorting algorithm of your choice.
        //- Print the sorted "numbers" array.
        for (int i = 0; i < numbers.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[j] < numbers[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = numbers[i];
            numbers[i] = numbers[minIndex];
            numbers[minIndex] = temp;
        }

        for (int num : numbers) {
            System.out.print(num + " ");
        }
        System.out.println();

    }
}